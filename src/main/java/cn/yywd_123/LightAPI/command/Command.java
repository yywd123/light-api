package cn.yywd_123.LightAPI.command;

public abstract class Command {
    private final String[] key;

    public Command(String[] key) {
        this.key = key;
    }

    public String[] getKey() {
        return key;
    }

    public abstract void exec(String[] args);
}
